(This is a meme from [Catpuccin](https://github.com/catppuccin/catppuccin)'s discord)

<p align="center">
	<img src="https://gitlab.com/iruzo/winstoner/-/raw/main/example.png"/>
</p>

# Info

Blazingly fast C program that will "winstify" your words.

# Usage

```
./winstoner [word1] [word2] [word3] ...
```

* last binary in artifacts
